package com.caigaoqinng.tech.config;

import com.caigaoqing.tech.common.support.RequestBodyWrapFactoryBean;
import com.caigaoqing.tech.common.support.ResponseBodyWrapFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Administrator on 2018/8/17.
 */
@Configuration
public class OpenApiRegisterBean {

    /**
     * @Author 蔡高情
     * @Description响应数据加密
     * @Date 13:48 2018/7/25 0025
     * @Param
     * @return
     **/
    @Bean
    public ResponseBodyWrapFactoryBean getResponseBodyWrap() {
        return new ResponseBodyWrapFactoryBean();
    }
    /**
     * @Author 蔡高情
     * @Description  接收数据解密
     * @Date 13:49 2018/7/25 0025
     * @Param []
     * @return
     **/
    @Bean
    public RequestBodyWrapFactoryBean getRequestBodyWrap() {
        return new RequestBodyWrapFactoryBean();
    }

}
