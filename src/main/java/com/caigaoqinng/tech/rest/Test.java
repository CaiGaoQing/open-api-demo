package com.caigaoqinng.tech.rest;

import com.caigaoqing.tech.common.annotation.RequestBodyDecrypt;
import com.caigaoqing.tech.common.annotation.ResponseBodyEncrypt;

import com.caigaoqing.tech.common.entity.MapWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * Created by Administrator on 2018/8/17.
 */
@Slf4j
@RestController
@RequestMapping("/api/")
public class Test {
    /**
     * 测试传递 不可以接受get，只能接受post参数 所有get参数用原来的进行解析  get参数不加密传输
     * @return
     */
    @ResponseBodyEncrypt()
    @PostMapping("test")
    public String getMap(String age,@RequestBodyDecrypt String userName){
        log.info(userName);
        return userName;
    }

    /**
     * 测试传递 不可以接受get，只能接受post参数 所有get参数用原来的进行解析  get参数不加密传输
     * @return
     */
    @ResponseBodyEncrypt()
    @PostMapping("testMap")
    public Map getMap(@RequestBodyDecrypt MapWrapper userName){
        log.info(userName.toString());
        return userName.toMap();
    }

}
