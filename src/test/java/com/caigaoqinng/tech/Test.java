package com.caigaoqinng.tech;

import com.alibaba.fastjson.JSON;
import com.caigaoqing.tech.util.encrypt.AesCBC;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2018/8/17.
 */
public class Test {
    public static void main(String[] args) throws Exception{
        String data= AesCBC.encrypt("123","1234567812345678","1234567812345678");
        Map map =new HashMap<>();
        map.put("name","caigaoqing");
        map.put("age",21);
        String dataStr=JSON.toJSON(map).toString();
        String data1= AesCBC.encrypt(dataStr,"1234567812345678","1234567812345678");

        System.out.println("要请求的数据1         "+data);
        System.out.println("要请求的数据2         "+data1);

        String result=AesCBC.decrypt(data,"1234567812345678","1234567812345678");
        String result1=AesCBC.decrypt(data1,"1234567812345678","1234567812345678");
        System.out.println("应该返回的数据1         "+result);

        System.out.println("应该返回的数据2         "+result1);
    }
}
