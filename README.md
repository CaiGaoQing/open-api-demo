测试代码

1.把定义的2个类加入到spring中

    package com.caigaoqinng.tech.config;
    
    import com.caigaoqing.tech.common.support.RequestBodyWrapFactoryBean;
    import com.caigaoqing.tech.common.support.ResponseBodyWrapFactoryBean;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    
    /**
     * Created by Administrator on 2018/8/17.
     */
    @Configuration
    public class OpenApiRegisterBean {
    
        /**
         * @Author 蔡高情
         * @Description响应数据加密
         * @Date 13:48 2018/7/25 0025
         * @Param
         * @return
         **/
        @Bean
        public ResponseBodyWrapFactoryBean getResponseBodyWrap() {
            return new ResponseBodyWrapFactoryBean();
        }
        /**
         * @Author 蔡高情
         * @Description  接收数据解密
         * @Date 13:49 2018/7/25 0025
         * @Param []
         * @return
         **/
        @Bean
        public RequestBodyWrapFactoryBean getRequestBodyWrap() {
            return new RequestBodyWrapFactoryBean();
        }
    
    }



常规项目配置xml <bean id=xxx  class=xxx/>

2.填写测试类（web）

    package com.caigaoqinng.tech.rest;
    
    import com.caigaoqing.tech.common.annotation.RequestBodyDecrypt;
    import com.caigaoqing.tech.common.annotation.ResponseBodyEncrypt;
    
    import com.caigaoqing.tech.common.entity.MapWrapper;
    import lombok.extern.slf4j.Slf4j;
    import org.springframework.web.bind.annotation.PostMapping;
    import org.springframework.web.bind.annotation.RequestMapping;
    import org.springframework.web.bind.annotation.RestController;
    
    import java.util.Map;
    
    
    /**
     * Created by Administrator on 2018/8/17.
     */
    @Slf4j
    @RestController
    @RequestMapping("/api/")
    public class Test {
        /**
         * 测试传递 不可以接受get，只能接受post参数 所有get参数用原来的进行解析  get参数不加密传输
         * @return
         */
        @ResponseBodyEncrypt()
        @PostMapping("test")
        public String getMap(String age,@RequestBodyDecrypt String userName){
            log.info(userName);
            return userName;
        }
    
        /**
         * 测试传递 不可以接受get，只能接受post参数 所有get参数用原来的进行解析  get参数不加密传输
         * @return
         */
        @ResponseBodyEncrypt()
        @PostMapping("testMap")
        public Map getMap(@RequestBodyDecrypt MapWrapper userName){
            log.info(userName.toString());
            return userName.toMap();
        }
    
    }



3加密测试

    package com.caigaoqinng.tech;
    
    import com.alibaba.fastjson.JSON;
    import com.caigaoqing.tech.util.encrypt.AesCBC;
    
    import java.util.HashMap;
    import java.util.Map;
    
    /**
     * Created by Administrator on 2018/8/17.
     */
    public class Test {
        public static void main(String[] args) throws Exception{
            String data= AesCBC.encrypt("123","1234567812345678","1234567812345678");
            Map map =new HashMap<>();
            map.put("name","caigaoqing");
            map.put("age",21);
            String dataStr=JSON.toJSON(map).toString();
            String data1= AesCBC.encrypt(dataStr,"1234567812345678","1234567812345678");
    
            System.out.println("要请求的数据1         "+data);
            System.out.println("要请求的数据2         "+data1);
    
            String result=AesCBC.decrypt(data,"1234567812345678","1234567812345678");
            String result1=AesCBC.decrypt(data1,"1234567812345678","1234567812345678");
            System.out.println("应该返回的数据1         "+result);
    
            System.out.println("应该返回的数据2         "+result1);
        }
    }



4.postman测试发送加密数据，返回加密数据


